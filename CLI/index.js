// -------------------------------
// MODULES
// -------------------------------
const _fs = require('fs');
const _readline = require('readline');
const _spawnSync = require('child_process').spawnSync;

// -------------------------------
// UTILS
// -------------------------------
function _inputSync(question, inputValidator) {
    return new Promise((resolve, reject) => {
        const input = _readline.createInterface({
            input: process.stdin,
            output: process.stdout,
        });
        try {
            input.question(question, async (answer) => {
                if (inputValidator) {
                    const isInvalid = inputValidator(answer);
                    if (isInvalid) {
                        const newAnswer = await _inputSync(question, inputValidator);
                        resolve(newAnswer);
                        input.close();
                        return;
                    }
                }
                resolve(answer);
                input.close();
            });
        } catch (e) {
            reject(e);
        }
    });
}
function _cmd(command) {
    _spawnSync(command, {
        shell: true,
        stdio: ['inherit', 'inherit', 'inherit'],
    });
}
function _isNecessary(inputAnswer) {
    return inputAnswer
        ? ['yes', 'y'].includes(inputAnswer.toLowerCase())
        : false;
}

// -------------------------------
// ACTIONS
// -------------------------------
async function initProject() {
    // 1. create package.json
    _cmd(`npm create`);

    // 2. update package.json and install packages
    const toUpdatePackage = await _inputSync(`Update scripts and packages in package.json? [Y/n]: `);
    if (_isNecessary(toUpdatePackage)) {
        const packageJson = JSON.parse(_fs.readFileSync(`./package.json`, { encoding: 'utf-8' }));
        packageJson.main = "index.js";
        packageJson.scripts = {
            serve: "nodemon index.js",
            "serve-prod": "node index.js"
        };
        packageJson.dependencies = {
            cors: "^2.8.5",
            express: "^4.17.2"
        };
        packageJson.devDependencies = {
            nodemon: "^2.0.15"
        };
        _fs.writeFileSync(`./package.json`, JSON.stringify(packageJson, null, 2), { encoding: 'utf-8' });
        _cmd(`npm i`);
    }

    // 3. create files and folders
    _fs.copyFileSync(`./CLI/dumps/index.js.dump`, `index.js`);
    _fs.mkdirSync(`./src/routers`, { recursive: true });
    _fs.mkdirSync(`./src/controllers`, { recursive: true });
    _fs.mkdirSync(`./src/services`, { recursive: true });
    _fs.mkdirSync(`./src/utils`, { recursive: true });
    _fs.copyFileSync(`./CLI/dumps/helpers.js.dump`, `./src/utils/helpers.js`);
    const createExampleFiles = await _inputSync(`Create example files? [Y/n]: `);
    if (_isNecessary(createExampleFiles)) {
        _fs.copyFileSync(`./CLI/dumps/controller-example.js.dump`, `./src/controllers/Example.js`);
        _fs.copyFileSync(`./CLI/dumps/route-example.js.dump`, `./src/routers/example.js`);
        _fs.copyFileSync(`./CLI/dumps/route-index.js.dump`, `./src/routers/index.js`);
    }
}

(async () => {
    await initProject();
})();
