# express-heroku

Learning to deploy Express.js Application on Heroku 

## Heroku

## Express.js

If you are already know the Express.js maybe can just copy files below to new project folder and run CLI.bat
* CLI.bat
* CLI/*


1. Create Project
   ```js
   {
      "scripts": {
         "serve": "nodemon index.js", // hotreload for local develop
         "build": "node index.js"
      }
   }
   ```
2. Install `cors` and `express` packages
   ```shell
   npm i cors express --save
   ```
3. Install `nodemeon` for hot-reload
   ```shell
   npm i nodemon --save-dev
   ```
4. Create required files and folders

### index.js

