// -------------------------------
// MODULES
// -------------------------------
const _fs = require('fs');
const _readline = require('readline');
const _spawnSync = require('child_process').spawnSync;

// -------------------------------
// UTILS
// -------------------------------
function _inputSync(question, inputValidator) {
    return new Promise((resolve, reject) => {
        const input = _readline.createInterface({
            input: process.stdin,
            output: process.stdout,
        });
        try {
            input.question(question, async (answer) => {
                if (inputValidator) {
                    const isInvalid = inputValidator(answer);
                    if (isInvalid) {
                        const newAnswer = await _inputSync(question, inputValidator);
                        resolve(newAnswer);
                        input.close();
                        return;
                    }
                }
                resolve(answer);
                input.close();
            });
        } catch (e) {
            reject(e);
        }
    });
}
function _cmd(command) {
    _spawnSync(command, {
        shell: true,
        stdio: ['inherit', 'inherit', 'inherit'],
    });
}
function _isNecessary(inputAnswer) {
    return inputAnswer
        ? ['yes', 'y'].includes(inputAnswer.toLowerCase())
        : false;
}

// -------------------------------
// ACTIONS
// -------------------------------

async function initHeroku() {
    // 1. load package.json
    const packageJson = JSON.parse(_fs.readFileSync(`./package.json`, { encoding: 'utf-8' }));

    // 2. login Heroku
    _cmd(`heroku login -i`);

    // 3. create Heroku App
    const projectName = packageJson.name;
    const herokuAppName = await _inputSync(`Heroku App name (default as ${projectName})`) || projectName;
    _cmd(`heroku create ${herokuAppName}`);

    // 4. Create heroku remote on GIT repository
    _cmd(`heroku git:remote -a ${herokuAppName}`);

    // 5. Create Procfile for Heroku to startup application
    _fs.writeFileSync(`./Procfile`, `web: npm run serve-prod`, { encoding: 'utf-8' });

    // 6. Set Heroku using port (cannot be 80 or 443)
    _cmd(`heroku config:set PORT=8000`);
}

(async () => {
    await initHeroku();
})();
